Python training UGA 2018
========================

Day 0
-----

- `Introduction <introduction.slides.html>`_
- `pres00 intro + first step <pres00_intro_first_steps.slides.html>`_ (~ 1h)
- `pres01 intro_language <pres01_intro_language.slides.html>`_ (~ 1h30)
- `pres02 basic_statements <pres02_basic_statements.slides.html>`_ (~ 2h30)
- `pres030 functions <pres030_functions.slides.html>`_ (~ 30mn)
- `pres031 functions_testing <pres031_functions_testing.slides.html>`_ (~30mn)
- `pres032 functions_final <pres032_functions_final.slides.html>`_ (~30mn)  

Day 1
-----

- `pres04_readwritefiles <pres04_readwritefiles.slides.html>`_ (20 min)
- `pres05_practical0 <pres05_practical0.slides.html>`_ (2h30)
- `pres06_import_standard_library <pres06_import_standard_library.slides.html>`_ (1h)
- `pres07_data_struct <pres07_data_struct.slides.html>`_ (~40 min)
- `pres080_oop_encapsulation <pres080_oop_encapsulation.slides.html>`_ (~20 min)
- `pres081_oop_inheritance <pres081_oop_inheritance.slides.html>`_ (~20 min)
- `pres09_practical1 <pres09_practical1.slides.html>`_ (2h30)

Day 2
-----

- `pres10_environnement <pres10_environnement.slides.html>`_
- `pres110_intro_numpy_scipy_pandas <pres110_intro_numpy_scipy_pandas.slides.html>`_
- `pres111_intro_matplotlib <pres111_intro_matplotlib.slides.html>`_
- `pres12_practical2 <pres12_practical2.slides.html>`_
- `pres13_doc_applications <pres13_doc_applications.slides.html>`_
- `pres15_data_manipulation_plotting <pres15_practical5.slides.html>`_
- `practical_numpy_img_median <practical_numpy_img_median.slides.html>`_
