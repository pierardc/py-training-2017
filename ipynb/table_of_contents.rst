Table of contents
=================

00. Introduction first steps

    - Why Python?
    - Python: a language, an interpreter
    - Remark Python 2 / Python 3
    - How to write and run Python code
      * Execute a script with the command python
      * Work interactively with ipython
      * Python in an IDE (Spyder)
      * Python in the browser (Jupyter)

01. Introduction language Python

    - Characteristics of the Python language
      * ...
      * PEP8
      * Dynamically and strongly typed: objects and variables
      * Spaces for objects and variables (names)
      * You have to use a good editor

02. Basic statements

    - Simple types: `int`, `float`, `bool`, `True`, `False`, `None`, `and`,
      `or`, `not` and other boolean operators (`is` and `id` keywords)
    - `str`
    - `list` (first introduction, `in`)
    - Mutable / immutable
    - References (`del`)
    - Slicing
    - The function `range`
    - Conditions (`if`, `elif`, `else`)
    - Loops (`while`, `for`, `continue`, `break`)
    - `try`, `except`, `raise`

03. Functions

    - Function definition (`def`, `return`, `lambda`)
    - Namespaces and Function call
    - Example of the print function

04. Read / write files

    - `open`, read, write
    - Context: `with` keyword

05. Practical work 0

06. Import statement and the standard library

    - `import` and `from` keywords
    - Multifile programming
    - The standard library

07. Data structures

    - `list`
    - `tuple`
    - `set`
    - `dict`

08. Object-oriented programming

    - Definition, `class` keyword
    - ...

09. Practical work 1 (import, data structures and OOP)

10. Python environments

    - Different implementations (CPython, Pypy, Jython, IronPython, Pyston, ...)
    - (C) extensions
    - Internal and external packages
    - PyPI - the Python Package Index
    - Installation and environment tools (pip, virtualenv, conda)
    - Packaging (setup.py, setuptools)

11. Introduction to Numpy and Matplotlib

12. Practical work 2 (Image processing with numpy & matplotlib, argparse,
    simple testing and packaging)

13. Documentations and applications

    - Where do I find help, documentation and tutorials?
    - List of possible applications and tools...

      * web (Django, ...)
      * presentations (jupyter)
      * basic computation and plot (numpy, scipy, matplotlib, ...)
      * image processing (scikit-image)
      * big data, machine learning (scikit-learn)
      * symbolic mathematic (Sympy)
      * control of physical laboratory experiments (modbus, gpib, fluidlab, ...)
      * grid computing ()
      * glue language (C, C++, Fortran)
      * high performance computing (Cython, Pythran, Numba, mpi4py, ...)
      * ...

14. Advanced Python language

    - iterators and `yield` keyword
    - decorators
