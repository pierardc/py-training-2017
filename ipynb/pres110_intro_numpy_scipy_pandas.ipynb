{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Python training UGA 2017\n",
    "\n",
    "**A training to acquire strong basis in Python to use it efficiently**\n",
    "\n",
    "Pierre Augier (LEGI), Cyrille Bonamy (LEGI), Eric Maldonado (Irstea), Franck Thollard (ISTerre), Oliver Henriot (GRICAD), Christophe Picard (LJK), Loïc Huder (ISTerre)\n",
    "\n",
    "# Python scientific ecosystem\n",
    "# A short introduction to Numpy, Scipy and Pandas"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Python scientific ecosystem\n",
    "\n",
    "There are a lot of very good Python packages for sciences. The fundamental packages are in particular:\n",
    "- [numpy](http://www.numpy.org/): numerical computing with powerful numerical arrays objects, and routines to manipulate them.\n",
    "- [scipy](http://www.scipy.org/): high-level numerical routines. Optimization, regression, interpolation, etc.\n",
    "- [matplotlib](http://matplotlib.org/): 2D-3D visualization, “publication-ready” plots.\n",
    "\n",
    "With `IPython` and `Spyder`, Python plus these fundamental scientific packages constitutes a very good alternative to Matlab, that is technically very similar (using the libraries Blas and Lapack)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "source": [
    "Matlab has a JustInTime (JIT) compiler so that Matlab code is generally faster than Python. However, we will see that Numpy is already quite efficient for standard operations and other Python tools (for example `pypy`, `cython`, `numba`, `pythran`, `theano`...) can be used to optimize the code to reach the performance of optimized Matlab code.\n",
    "\n",
    "The advantage of Python over Matlab is its high polyvalency (and nicer syntax) and there are notably several other scientific Python packages (see our notebook `pres13_doc_applications.ipynb`):\n",
    "- [sympy](http://www.sympy.org) for symbolic computing,\n",
    "- [pandas](http://pandas.pydata.org/), [statsmodels](http://www.statsmodels.org), [seaborn](http://seaborn.pydata.org/) for statistics,\n",
    "- [h5py](http://www.h5py.org/), [h5netcdf](https://pypi.python.org/pypi/h5netcdf) for hdf5 and netcdf files,\n",
    "- [mpi4py](https://pypi.python.org/pypi/mpi4py) for MPI communications,\n",
    "- [opencv](https://pypi.python.org/pypi/opencv-python), [scikit-image](http://scikit-image.org/) for image processing,\n",
    "- [pyopencl](https://pypi.python.org/pypi/pyopencl), [pycuda](https://mathema.tician.de/software/pycuda/), [theano](http://deeplearning.net/software/theano/), [tensorflow](https://www.tensorflow.org/) for speed and GPU computing,\n",
    "- [scikit-learn](http://scikit-learn.org), [keras](https://keras.io/), [mxnet](http://mxnet.io/) for machine learning,\n",
    "- [bokeh](http://bokeh.pydata.org) for display data efficiently,\n",
    "- [mayavi](http://docs.enthought.com/mayavi/mayavi/) for 3D visualization,\n",
    "- [qtpy](https://pypi.python.org/pypi/QtPy), [kivy](https://kivy.org) for GUI frameworks\n",
    "- ..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## A short introduction on NumPy\n",
    "\n",
    "Code using `numpy` usually starts with the import statement"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "NumPy provides the type `np.ndarray`. Such arrays are multidimensionnal sequences of homogeneous elements (numbers) to represent vectors, matrices, tensors..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Array creation\n",
    "NumPy arrays can be created in several ways:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([10. , 12.5, 15. , 17.5, 20. ])"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# from a list\n",
    "l = [10.0, 12.5, 15.0, 17.5, 20.0]\n",
    "np.array(l)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([1.75274491e-316, 6.94225492e-310, 6.94224527e-310, 6.94225376e-310])"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# fast but the values can be anything\n",
    "np.empty(4)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {
    "scrolled": true,
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([[0., 0., 0., 0., 0., 0.],\n",
       "       [0., 0., 0., 0., 0., 0.]])"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Filled with zeros (slower than np.empty)\n",
    "np.zeros([2, 6])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "(2, 3, 4) 24 float64\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "array([[[1., 1., 1., 1.],\n",
       "        [1., 1., 1., 1.],\n",
       "        [1., 1., 1., 1.]],\n",
       "\n",
       "       [[1., 1., 1., 1.],\n",
       "        [1., 1., 1., 1.],\n",
       "        [1., 1., 1., 1.]]])"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Multidimensional array filled with ones \n",
    "a = np.ones([2, 3, 4])\n",
    "print(a.shape, a.size, a.dtype)\n",
    "a"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Generate sequences"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([0, 1, 2, 3])"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Like range but produces a 1D numpy array\n",
    "np.arange(4)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([2. , 2.1, 2.2, 2.3, 2.4, 2.5, 2.6, 2.7, 2.8, 2.9, 3. , 3.1, 3.2,\n",
       "       3.3, 3.4, 3.5, 3.6, 3.7, 3.8, 3.9])"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Start and step can be changed\n",
    "np.arange(2., 4., 0.1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([10. , 12.5, 15. , 17.5, 20. ])"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Equally-spaced elements between start and end (included)\n",
    "np.linspace(10, 20, 5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "A NumPy array can be easily converted to a Python list."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[10.0, 12.5, 15.0, 17.5, 20.0]"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "a = np.linspace(10, 20 ,5)\n",
    "a.tolist()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Manipulating NumPy arrays\n",
    "\n",
    "### Access elements\n",
    "Elements in a `numpy` array can be accessed using indexing and slicing in any dimension. It also offers the same functionalities available in Fortan or Matlab."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Indexes and slices\n",
    "For example, we can create an array `A` and perform any kind of selection operations on it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([[0.74931905, 0.4399789 , 0.96017188, 0.88886798, 0.28382067],\n",
       "       [0.4532329 , 0.99181478, 0.07017858, 0.4993961 , 0.1678844 ],\n",
       "       [0.59791893, 0.50793759, 0.77954852, 0.05390075, 0.984206  ],\n",
       "       [0.93149267, 0.02959492, 0.60720976, 0.92916837, 0.24923606]])"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "A = np.random.random([4, 5])\n",
    "A"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0.45323290450951004"
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Get the element from second line, first column\n",
    "A[1, 0]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([[0.74931905, 0.4399789 , 0.96017188, 0.88886798, 0.28382067],\n",
       "       [0.4532329 , 0.99181478, 0.07017858, 0.4993961 , 0.1678844 ]])"
      ]
     },
     "execution_count": 12,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Get the first two lines\n",
    "A[:2]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([0.28382067, 0.1678844 , 0.984206  , 0.24923606])"
      ]
     },
     "execution_count": 13,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Get the last column\n",
    "A[:, -1]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([[0.74931905, 0.96017188, 0.28382067],\n",
       "       [0.4532329 , 0.07017858, 0.1678844 ]])"
      ]
     },
     "execution_count": 14,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Get the first two lines and the columns with an even index\n",
    "A[:2, ::2]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Using a mask to select elements validating a condition:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[ True False  True  True False]\n",
      " [False  True False False False]\n",
      " [ True  True  True False  True]\n",
      " [ True False  True  True False]]\n",
      "[0.74931905 0.96017188 0.88886798 0.99181478 0.59791893 0.50793759\n",
      " 0.77954852 0.984206   0.93149267 0.60720976 0.92916837]\n"
     ]
    }
   ],
   "source": [
    "cond = A > 0.5\n",
    "print(cond)\n",
    "print(A[cond])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The mask is in fact a particular case of the advanced indexing capabilities provided by NumPy. For example, it is even possible to use lists for indexing:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[0.74931905 0.4399789  0.96017188 0.88886798 0.28382067]\n",
      " [0.4532329  0.99181478 0.07017858 0.4993961  0.1678844 ]\n",
      " [0.59791893 0.50793759 0.77954852 0.05390075 0.984206  ]\n",
      " [0.93149267 0.02959492 0.60720976 0.92916837 0.24923606]]\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "array([[0.74931905, 0.4399789 , 0.28382067],\n",
       "       [0.4532329 , 0.99181478, 0.1678844 ],\n",
       "       [0.59791893, 0.50793759, 0.984206  ],\n",
       "       [0.93149267, 0.02959492, 0.24923606]])"
      ]
     },
     "execution_count": 16,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Selecting only particular columns\n",
    "print(A)\n",
    "A[:, [0, 1, 4]]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Perform array manipulations\n",
    "### Apply arithmetic operations to whole arrays (element-wise):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([[33.05466955, 29.59337041, 35.52364888, 34.67876614, 27.91876089],\n",
       "       [29.73774911, 35.90184432, 25.7067108 , 30.24335742, 26.70702917],\n",
       "       [31.3366963 , 30.33737648, 33.4031811 , 25.54191284, 35.81072142],\n",
       "       [35.18260526, 25.29682501, 31.44080124, 35.15503757, 27.55447916]])"
      ]
     },
     "execution_count": 17,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "(A+5)**2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Apply functions element-wise:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([[2.11555894, 1.55267445, 2.61214542, 2.43237461, 1.32819473],\n",
       "       [1.57339059, 2.6961229 , 1.07269972, 1.6477259 , 1.18279987],\n",
       "       [1.81833078, 1.66186022, 2.1804876 , 1.05537986, 2.67568654],\n",
       "       [2.53829518, 1.0300372 , 1.8353033 , 2.53240228, 1.28304487]])"
      ]
     },
     "execution_count": 18,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "np.exp(A) # With numpy arrays, use the functions from numpy !"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## NumPy efficiency\n",
    "\n",
    "In addition of being extremely convenient to manipulate arrays of arbitrary dimensions, `numpy` is also much more efficient than pure Python.\n",
    "\n",
    "### Array creation:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {},
   "outputs": [],
   "source": [
    "n = 1000"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "%%capture timeit_python\n",
    "# to capture the result of the command timeit in the variable timeit_python\n",
    "# Pure Python\n",
    "%timeit list(range(n))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%capture timeit_numpy\n",
    "# numpy\n",
    "%timeit np.arange(n)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "outputs": [],
   "source": [
    "def compute_time_in_second(timeit_result):\n",
    "    string = timeit_result.stdout\n",
    "    print(string)\n",
    "    for line in string.split('\\n'):\n",
    "        words = line.split(' ')\n",
    "        if len(words) > 1:\n",
    "            time = float(words[0])\n",
    "            unit = words[1]\n",
    "    if unit == 'ms':\n",
    "        time *= 1e-3\n",
    "    elif unit == 'us':\n",
    "        time *= 1e-6\n",
    "    elif unit == 'ns':\n",
    "        time *= 1e-9\n",
    "    return time\n",
    "\n",
    "def compare_times(string, timeit_python, timeit_numpy):\n",
    "    time_python = compute_time_in_second(timeit_python)\n",
    "    time_numpy = compute_time_in_second(timeit_numpy)\n",
    "\n",
    "    print(string + ': ratio times (Python / NumPy): ', \n",
    "          time_python/time_numpy)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "14.1 us +- 1.04 us per loop (mean +- std. dev. of 7 runs, 100000 loops each)\n",
      "\n",
      "1.63 us +- 94.5 ns per loop (mean +- std. dev. of 7 runs, 1000000 loops each)\n",
      "\n",
      "Creation of object: ratio times (Python / NumPy):  8.650306748466258\n"
     ]
    }
   ],
   "source": [
    "compare_times('Creation of object', timeit_python, timeit_numpy)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Array operations:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "metadata": {},
   "outputs": [],
   "source": [
    "n = 200000\n",
    "python_r_1 = range(n)\n",
    "python_r_2 = range(n)\n",
    "\n",
    "numpy_a_1 = np.arange(n)\n",
    "numpy_a_2 = np.arange(n)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%capture timeit_python\n",
    "%%timeit\n",
    "# Regular Python\n",
    "[(x + y) for x, y in zip(python_r_1, python_r_2)]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 26,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%capture timeit_numpy\n",
    "%%timeit\n",
    "#Numpy\n",
    "numpy_a_1 + numpy_a_2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 27,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "20.1 ms +- 885 us per loop (mean +- std. dev. of 7 runs, 10 loops each)\n",
      "\n",
      "276 us +- 25.5 us per loop (mean +- std. dev. of 7 runs, 1000 loops each)\n",
      "\n",
      "Additions: ratio times (Python / NumPy):  72.82608695652175\n"
     ]
    }
   ],
   "source": [
    "compare_times('Additions', timeit_python, timeit_numpy)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This shows that when you need to perform mathematical operations on a lot of homogeneous numbers, it is more efficient to use `numpy` arrays."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Setting parts of arrays"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 28,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[0.         0.4399789  0.96017188 0.88886798 0.28382067]\n",
      " [0.         0.99181478 0.07017858 0.4993961  0.1678844 ]\n",
      " [0.         0.50793759 0.77954852 0.05390075 0.984206  ]\n",
      " [0.         0.02959492 0.60720976 0.92916837 0.24923606]]\n"
     ]
    }
   ],
   "source": [
    "A[:, 0] = 0.\n",
    "print(A)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 29,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[ 0.          2.27283627  1.04148019  1.12502646  3.52335153]\n",
      " [ 0.          1.00825277 14.24936289  2.00241854  5.95647958]\n",
      " [ 0.          1.96874581  1.28279379 18.55261602  1.01604746]\n",
      " [ 0.         33.78958815  1.64687736  1.07623121  4.01226058]]\n"
     ]
    }
   ],
   "source": [
    "# BONUS: Safe element-wise inverse with masks\n",
    "cond = (A != 0)\n",
    "A[cond] = 1./A[cond]\n",
    "print(A)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Attributes and methods of `np.ndarray` (see the [doc](https://docs.scipy.org/doc/numpy/reference/generated/numpy.ndarray.html#numpy.ndarray))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 30,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "['T', 'all', 'any', 'argmax', 'argmin', 'argpartition', 'argsort', 'astype', 'base', 'byteswap', 'choose', 'clip', 'compress', 'conj', 'conjugate', 'copy', 'ctypes', 'cumprod', 'cumsum', 'data', 'diagonal', 'dot', 'dtype', 'dump', 'dumps', 'fill', 'flags', 'flat', 'flatten', 'getfield', 'imag', 'item', 'itemset', 'itemsize', 'max', 'mean', 'min', 'nbytes', 'ndim', 'newbyteorder', 'nonzero', 'partition', 'prod', 'ptp', 'put', 'ravel', 'real', 'repeat', 'reshape', 'resize', 'round', 'searchsorted', 'setfield', 'setflags', 'shape', 'size', 'sort', 'squeeze', 'std', 'strides', 'sum', 'swapaxes', 'take', 'tobytes', 'tofile', 'tolist', 'tostring', 'trace', 'transpose', 'var', 'view']\n"
     ]
    }
   ],
   "source": [
    "print([s for s in dir(A) if not s.startswith('__')])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "#### Example 1: Get the mean through different dimensions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 31,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[ 0.          2.27283627  1.04148019  1.12502646  3.52335153]\n",
      " [ 0.          1.00825277 14.24936289  2.00241854  5.95647958]\n",
      " [ 0.          1.96874581  1.28279379 18.55261602  1.01604746]\n",
      " [ 0.         33.78958815  1.64687736  1.07623121  4.01226058]]\n",
      "Mean value 4.7262184313320255\n",
      "Mean line [0.         9.75985575 4.55512856 5.68907306 3.62703479]\n",
      "Mean column [1.59253889 4.64330276 4.56404062 8.10499146]\n"
     ]
    }
   ],
   "source": [
    "print(A)\n",
    "print('Mean value', A.mean())\n",
    "print('Mean line', A.mean(axis=0))\n",
    "print('Mean column', A.mean(axis=1))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "#### Example 2: Manipulate the shape of arrays while keeping all elements"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 32,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[ 0.          2.27283627  1.04148019  1.12502646  3.52335153]\n",
      " [ 0.          1.00825277 14.24936289  2.00241854  5.95647958]\n",
      " [ 0.          1.96874581  1.28279379 18.55261602  1.01604746]\n",
      " [ 0.         33.78958815  1.64687736  1.07623121  4.01226058]] (4, 5)\n",
      "[ 0.          2.27283627  1.04148019  1.12502646  3.52335153  0.\n",
      "  1.00825277 14.24936289  2.00241854  5.95647958  0.          1.96874581\n",
      "  1.28279379 18.55261602  1.01604746  0.         33.78958815  1.64687736\n",
      "  1.07623121  4.01226058] (20,)\n"
     ]
    }
   ],
   "source": [
    "print(A, A.shape)\n",
    "A_flat = A.flatten()\n",
    "print(A_flat, A_flat.shape)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 33,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[ 0.          2.27283627  1.04148019  1.12502646  3.52335153]\n",
      " [ 0.          1.00825277 14.24936289  2.00241854  5.95647958]\n",
      " [ 0.          1.96874581  1.28279379 18.55261602  1.01604746]\n",
      " [ 0.         33.78958815  1.64687736  1.07623121  4.01226058]] (4, 5)\n"
     ]
    }
   ],
   "source": [
    "new_A = A_flat.reshape((4, 5))\n",
    "print(new_A, new_A.shape)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Remark: matrix/dot product\n",
    "\n",
    "Since Python 3.5, it exists the operator `@` that performs matrix and dot products (previously only available through `np.matmul` and `np.dot`)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 34,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[ 0.  1.  2.  3.  4.  5.  6.  7.  8.  9. 10.]\n",
      "385.0\n"
     ]
    }
   ],
   "source": [
    "b = np.linspace(0, 10, 11)\n",
    "c = b @ b\n",
    "\n",
    "print(b)\n",
    "print(c)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 35,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[1. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0.]\n",
      " [0. 1. 0. 0. 0. 0. 0. 0. 0. 0. 0.]\n",
      " [0. 0. 1. 0. 0. 0. 0. 0. 0. 0. 0.]\n",
      " [0. 0. 0. 1. 0. 0. 0. 0. 0. 0. 0.]\n",
      " [0. 0. 0. 0. 1. 0. 0. 0. 0. 0. 0.]\n",
      " [0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0.]\n",
      " [0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0.]\n",
      " [0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0.]\n",
      " [0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0.]\n",
      " [0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0.]\n",
      " [0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0.]] [ 0.  1.  2.  3.  4.  5.  6.  7.  8.  9. 10.]\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "array([0., 1., 2., 3., 4., 0., 0., 0., 0., 0., 0.])"
      ]
     },
     "execution_count": 35,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "I = np.identity(11)\n",
    "I[5:, :] = 0.\n",
    "print(I, b)\n",
    "\n",
    "I @ b"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "#### For Matlab users\n",
    "\n",
    "|     ` `       | Matlab | Numpy |\n",
    "| ------------- | ------ | ----- |\n",
    "| element wise  |  `.*`  |  `*`  |\n",
    "|  dot product  |  `*`   |  `@`  |"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### To finish: `dtypes` and sub-packages"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`numpy` arrays can also be sorted, even when they are composed of complex data if the type of the columns are explicitly stated with `dtypes`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 36,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[(b'Germany', 233, 357021, 81799600)\n",
      " (b'United Kingdom', 256, 243610, 62262000)\n",
      " (b'Belgium', 337,  30510, 11007020)\n",
      " (b'Netherlands', 393,  41526, 16928800)]\n"
     ]
    }
   ],
   "source": [
    "dtypes = np.dtype([('country', 'S20'), ('density', 'i4'), \n",
    "                  ('area', 'i4'), ('population', 'i4')])\n",
    "x = np.array([('Netherlands', 393, 41526, 16928800),\n",
    "              ('Belgium', 337, 30510, 11007020),\n",
    "              ('United Kingdom', 256, 243610, 62262000),\n",
    "              ('Germany', 233, 357021, 81799600)], \n",
    "             dtype=dtypes)\n",
    "arr = np.array(x, dtype=dtypes)\n",
    "arr.sort(order='density')\n",
    "print(arr)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "Dtypes are particularly useful when loading data of different types from a file with `np.genfromtxt`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 37,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([(7761., b'2016-01-01T01:00:00+01:00', 2. , 283.75, 94, 0.2, b'AJACCIO'),\n",
       "       (7761., b'2016-01-01T04:00:00+01:00', 2.2, 283.95, 91, 0.2, b'AJACCIO'),\n",
       "       (7761., b'2016-01-01T07:00:00+01:00', 1.7, 284.05, 88, 0.2, b'AJACCIO'),\n",
       "       ...,\n",
       "       (7761., b'2016-12-31T16:00:00+01:00', 2.2, 287.75, 61, 0. , b'AJACCIO'),\n",
       "       (7761., b'2016-12-31T19:00:00+01:00', 1.9, 284.05, 79, 0. , b'AJACCIO'),\n",
       "       (7761., b'2016-12-31T22:00:00+01:00', 2.5, 283.05, 79, 0. , b'AJACCIO')],\n",
       "      dtype=[('ID_OMM_station', '<f8'), ('Date', 'S25'), ('Average_wind_10_mn', '<f8'), ('Temperature', '<f8'), ('Humidity', '<i4'), ('Rainfall_3_last_hours', '<f8'), ('Station', 'S20')])"
      ]
     },
     "execution_count": 37,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "meteo_data = np.genfromtxt('../TP/TP1_MeteoData/data/synop-2016.csv', names=True, delimiter=',',\n",
    "                           dtype=('f8', 'S25', 'f8', 'f8', 'i4', 'f8', 'S20'))\n",
    "meteo_data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "#### NumPy and SciPy sub-packages:\n",
    "\n",
    "We already saw `numpy.random` to generate `numpy` arrays filled with random values. This submodule also provides functions related to distributions (Poisson, gaussian, etc.) and permutations."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "To perform linear algebra with dense matrices, we can use the submodule `numpy.linalg`. For instance, in order to compute the determinant of a random matrix, we use the method `det`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 38,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[0.97686707 0.19570122 0.36140422 0.06750466 0.18070765]\n",
      " [0.53024102 0.68681885 0.67799432 0.14903517 0.96101573]\n",
      " [0.41638762 0.08302575 0.37110877 0.04414894 0.49869079]\n",
      " [0.86026592 0.16876345 0.23845197 0.60002328 0.68178478]\n",
      " [0.9029329  0.68171618 0.35792988 0.09063473 0.78865979]]\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "0.046431202254328265"
      ]
     },
     "execution_count": 38,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "A = np.random.random([5,5])\n",
    "print(A)\n",
    "np.linalg.det(A)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "##### Matrix inversion (only on square matrices !)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 39,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[0.68681885 0.67799432]\n",
      " [0.08302575 0.37110877]]\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "array([[ 1.8686853 , -3.41398029],\n",
       "       [-0.41806881,  3.4584154 ]])"
      ]
     },
     "execution_count": 39,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "square_subA = A[1:3, 1:3]\n",
    "print(square_subA)\n",
    "np.linalg.inv(square_subA)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "If the data are sparse matrices, instead of using `numpy`, it is recommended to use the `sparse` subpackage of `scipy`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 40,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "  (0, 0)\t1\n",
      "  (0, 1)\t2\n",
      "  (1, 2)\t3\n",
      "  (2, 0)\t4\n",
      "  (2, 2)\t5\n"
     ]
    }
   ],
   "source": [
    "from scipy.sparse import csr_matrix\n",
    "print(csr_matrix([[1, 2, 0], [0, 0, 3], [4, 0, 5]]))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "#### SciPy or NumPy ?\n",
    "`scipy` also provides a submodule for linear algebra `scipy.linalg`. It provides an extension of `numpy.linalg`.\n",
    "\n",
    "For more info, see the related FAQ entry: https://www.scipy.org/scipylib/faq.html#why-both-numpy-linalg-and-scipy-linalg-what-s-the-difference."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Introduction to Pandas: Python Data Analysis Library\n",
    "\n",
    "Pandas is an open source library providing high-performance, easy-to-use data structures and data analysis tools for Python.\n",
    "\n",
    "[Pandas tutorial](https://pandas.pydata.org/pandas-docs/stable/10min.html)\n",
    "[Grenoble Python Working Session](https://github.com/iutzeler/Pres_Pandas/)\n",
    "[Pandas for SQL Users](https://hackernoon.com/pandas-cheatsheet-for-sql-people-part-1-2976894acd0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Some Solutions of Practical 1 with Pandas"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 41,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "32.60000000000002\n",
      "16.268433652530803\n",
      "2334.7\n",
      "30.5\n"
     ]
    }
   ],
   "source": [
    "import pandas as pd\n",
    "\n",
    "filename = \"../TP/TP1_MeteoData/data/synop-2016.csv\"\n",
    "\n",
    "df = pd.read_csv(filename, sep = ',', encoding = \"utf-8\", header=0)\n",
    "\n",
    "\"\"\"\n",
    "max temperature\n",
    "\"\"\"\n",
    "\n",
    "print(df['Temperature'].max() - 273.15)\n",
    "\n",
    "\"\"\"\n",
    "mean temperature\n",
    "\"\"\"\n",
    "print(df['Temperature'].mean() - 273.15)\n",
    "\n",
    "\"\"\"\n",
    "total rainfall\n",
    "\"\"\"\n",
    "print(df['Rainfall 3 last hours'].sum())\n",
    "\n",
    "\"\"\"\n",
    "August max temperature\n",
    "\n",
    "\"\"\"\n",
    "print(df[df['Date'].str.startswith('2016-08')]['Temperature'].max()-273.15)\n",
    "\n"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Diaporama",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
