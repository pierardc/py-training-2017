## To be done before the first day of the training session

We will have to use Python 3 (with Miniconda3), a good Python IDE (either
Spyder or VSCode), Jupyter and a version-control tool (Mercurial, or Git if you
know it and really like it).

In the following, we give indications about how to install these tools and how
to get the repository of this training locally on your computer. Please, try to
do this before the training and tell us if you encounter problems. You can fill
an issue
[here](https://gricad-gitlab.univ-grenoble-alpes.fr/python-uga/training-hpc/issues)
to explain what you did and what are the errors (please copy / paste the error
log).

Moreover, let's add that the best OS for HPC (and HPC with Python) is
Linux/GNU. Windows (at least without
[WLS](https://en.wikipedia.org/wiki/Windows_Subsystem_for_Linux)) and even
macOS are less adapted for this particular application. Python is a
cross-platform language but nevertheless, you will get a better experience for
HPC with Python on Linux.

#### Install Python and utilities

The first step is to install miniconda3 (see
[here](https://docs.conda.io/en/latest/miniconda.html))

```bash
wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
bash Miniconda3-latest-Linux-x86_64.sh
```

You have to answer "yes" to the last question about the initialization of
conda. When it's done, close the terminal (with `ctrl-d`) and reopen a new
terminal (with `ctrl-alt-t`). The world "(base)" should be in the line in the
terminal.

You will need to activate the conda channel `conda-forge` with:

```conda config --add channels conda-forge```

#### Install and setup Mercurial

To install and setup Mercurial, you can do:

```bash
pip install conda-app --no-cache-dir
conda-app install mercurial
```

Close the terminal (`ctrl-d`) and reopen a new terminal (`ctrl-alt-t`)!

```hg config --edit```

You have to write something like:

```
[ui]
username=myusername <email@adress.org>
editor=nano
tweakdefaults = True

[extensions]
hgext.extdiff =
# only to use Mercurial with GitHub and Gitlab
hggit =

[extdiff]
cmd.meld =
```

By default, Mercurial will open the program `vi`. It's highly probable that you
won't like it! Just exit and save the file by writing `:x` and pressing the
enter key. Then reopen the file `~\.hgrc` with another program (for example
using gedit with the command `gedit ~\.hgrc`).

Warning: Mercurial **has to be correctly setup**! Since we will use [the Gitlab
instance of UGA](https://gricad-gitlab.univ-grenoble-alpes.fr), the Mercurial
extension hg-git has to be activated so the line `hggit =` in the configuration
file is mandatory.

#### Clone this repository

Once everything is installed and setup, you should be able to clone the
repository of the training with:

```hg clone https://gricad-gitlab.univ-grenoble-alpes.fr/augierpi/intro-python.git```

Please tell us before the training if it does not work.

#### Install few packages in your base conda environment

```
cd intro-python
conda install --file requirements.txt
```

#### Check your environment

Once the repository is cloned you should have a new directory `training-hpc`
and you should be able to enter into it with `cd training-hpc`.

Finally, you can check that your Python environment is all fine with:

```python check_env.py```

#### Build the presentations

One needs Jupyter, rst2html5 (installable with `pip install rst2html5`), plus
`make` and few other Unix tools. Therefore, it is not easy to build the website
on Windows.

On Unix-like systems, `make presentations` should build the presentations and
`make serve` should start a server to visualize them in a browser.

#### Bonus

You can also install VSCode (see [here](https://code.visualstudio.com/download)).
