#!/usr/bin/env python3
#-*- coding: utf-8 -*-


class WeatherStation:
    def __init__(self):
        self.station_name = 'NOT INITIALISED'
        self.dates = []
        self.wind = []
        self.temperature = []
        self.humidity = []
        self.rainfall = []

    def load_data(self, file_path):
        """ Loading the data in multiple list attributes """
        with open(file_path) as f:
            f.readline()
            for line in f:
                value_fields = line.split(",")
                date = value_fields[1]
                # Format date to remove minutes and seconds
                formatted_date = date.split(":")[0]
                self.dates.append(formatted_date)
                # Add to attributes
                self.wind.append(float(value_fields[2]))
                self.temperature.append(float(value_fields[3]) - 273.15)
                self.humidity.append(float(value_fields[4]))
                raw_rainfall = value_fields[5]
                if raw_rainfall.strip() == "":
                    self.rainfall.append(0.)
                else:
                    self.rainfall.append(float(raw_rainfall))
                self.station_name = value_fields[6].strip()

    def get_max_temperature(self):
        """ Returns the date of max temperature and its value for one station """
        max_temperature = 0
        date_max_temperature = []
        for index, temp in enumerate(self.temperature):
            current_date = self.dates[index]
            if temp > max_temperature:
                max_temperature = temp
                date_max_temperature = [current_date]
            else:
                if temp == max_temperature:
                    date_max_temperature.append(current_date)
        return date_max_temperature, max_temperature

    def get_average_temperature(self):
        """ Returns the average temperature for one station """
        sum_temperatures = 0.0
        for temp in self.temperature:
            sum_temperatures += temp

        return sum_temperatures / len(self.temperature)

    def get_hours_humidity(self, rate):
        """ Returns the number of hours with humidity under a certain rate """
        period_under_rate = 0
        for humidity in self.humidity:
            if humidity < rate:
                period_under_rate = period_under_rate + 1

        return period_under_rate * 3

    def get_sum_rainfall(self):
        """ Returns the sum of precipitation for one station """
        sum_rainfall = 0.0
        for measured_rainfall in self.rainfall:
            sum_rainfall += measured_rainfall
        return sum_rainfall

    def period_without_rainfall(self):
        """ Returns the max period without rainfall """
        period_max = 0
        period = 0
        date = ""
        date_min = 0
        date_max = 0

        for index, measured_rainfall in enumerate(self.rainfall):
            current_date = self.dates[index]
            if float(measured_rainfall) <= 0.00:
                period = period + 1
            else:
                if period >= period_max:
                    period_max = period
                    date_min = date
                    date_max = current_date
                period = 0
                date = current_date

        return date_min, date_max, period_max/8


my_station = WeatherStation()
my_station.load_data('../data/synop-2016.csv')
print("Station :", my_station.station_name)

date_maxT, maxT = my_station.get_max_temperature()
print("the biggest temperature  @ ", my_station.station_name, " is:",
      maxT, "reached at", date_maxT)


print("the temperature average for ", my_station.station_name, " is: {0:.2f}"
      .format(my_station.get_average_temperature()))

print("the rainfall sum @ ", my_station.station_name, " is: {0:.2f}"
      .format(my_station.get_sum_rainfall()))
start_date, end_date, period = my_station.period_without_rainfall()
print("there is no rain from ", start_date, "to ", end_date, "so ", period, " days")
rate_humidity = 60
print("With a humidity inferior of ", rate_humidity, "% there are {0:.2f}"
      .format(my_station.get_hours_humidity(rate_humidity)), " hours")
print("##################\n")
