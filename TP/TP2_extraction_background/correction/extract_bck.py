#!/usr/bin/env python3
"""
Script for suppressing the background given a set of  images.

Algorithm:
    - get a list of images out of a directory
    - build a numpy array of shape (nb_image, width, height, color_dim)
    - computes the median value of each pixel (ie on the first dimension)

Note:
    - we consider here that all the images can lie in memory (ie the code will
      not scale with a very large set of images, or with images of large size).
"""

import numpy as np
from scipy import misc
import os
import sys
import logging
import argparse

# log level : DEBUG, INFO, WARNING, ERROR, CRITICAL
# change this to be less verbose when running it
logging.basicConfig(level=logging.DEBUG)


def load_img_set(img_list):
    """ load a set of images as a stacked numpy array

    :img_list: (iterable) the set of images to load
    :returns: a numpy array containing all the images. Shape is
    such that ret[k] = an image, i.e. a np array of shape on image.

    Precondition: img_list is assumed to be non empty
    Precondition: all the images should have the same shape

    Exception: RuntimeError is raised if all the images do not share the same shape
    """
    # memory allocation for the whole set

    # our problem now is to load all the images as a unique
    # numpy array. Fortunatelly, this exact question has been asked
    # on stackoverflow :
    # https://stackoverflow.com/questions/17394882/add-dimensions-to-a-numpy-array

    # compute the shape from the first image
    img_arr = misc.imread(os.path.join(args.directory, img_list[0]))
    shape0 = img_arr.shape

    all_frames = np.empty((len(img_list), shape0[0], shape0[1], 3))
    all_frames[0, :, :, :] = img_arr[:, :, :]

    # loading the images:
    for k, img_name in enumerate(img_list[1:]):
        img_arr = misc.imread(os.path.join(args.directory, img_name))
        if img_arr.shape != shape0:
            all_frames = None
            logging.error("image {} and {} do not share the same shape ({} vs {}".format(
                           img_list[0], img_name, shape0, img_arr.shape))
            raise RuntimeError("shape mismatch between images")
        all_frames[k, :, :, :] = img_arr[:, :, :]
    # now all_frames[i] is an image
    logging.debug("all frames loaded: all_frames shape is {}".format(all_frames.shape))
    return all_frames


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="extract background from a set of images")
    parser.add_argument("-d", "--directory", type=str, default=".",
                        help="the directory where to find the images (default .)")
    parser.add_argument("-o", "--output", type=str, default="median.jpg",
                        help="the output image (default median.jpg)")
    parser.add_argument("-p", "--pattern", type=str, default=".jpg",
                        help="pattern to look for (default=.jpg)")
    parser.add_argument("-H", action="store_true",
                        help="provides informations on the algorithm (and its limitations)")
    args = parser.parse_args()
    print(args)
    if args.H:
        print(__doc__)
        sys.exit(0)

    # get the list of images
    img_list = [x for x in os.listdir(args.directory) if x.endswith(args.pattern)]

    if len(img_list) == 0:
        logging.critical("no file found in {} with ending by {}, aborting",
                         args.directory, args.pattern)
        sys.exit(1)
    logging.info("%d images read", len(img_list))
    logging.debug("img list = {}".format(img_list))
    all_frames = None
    try:
        all_frames = load_img_set(img_list)
    except Exception as e:
        logging.critical("Cannot proceed ... {}".format(str(e)))

    # we are now in position to run compute the median for each pixel
    # see np.median
    median_img = np.median(all_frames, axis=0)

    # save the new image as jpeg:
    misc.imsave(args.output, median_img)
