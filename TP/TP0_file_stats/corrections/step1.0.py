#!/usr/bin/env python3
"""Computes basic statistics on file that contains a set of lines, each line
containing one float.

"""

file_name = '../data/file_with_comment_col0.txt'

total = 0.0
number = 0

with open(file_name) as handle:
    for line in handle:
        if line.startswith('#'):
            continue
        elem = float(line)
        total = total + elem
        number += 1


print('file = ' + file_name +
      '\nnb = {}; total = {:.2f}; avg = {:.2f}'.format(
          number, total, total/number))
