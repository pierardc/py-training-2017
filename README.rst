

Python training UGA 2017
========================

**A training to acquire strong basis in Python to use it efficiently**

The code of this training has been written for Python 3.

Formated output of the notebooks is available here: https://python-uga.gricad-pages.univ-grenoble-alpes.fr/py-training-2017


To display and/or modify the presentations, use the Makefile. See::

  make help

Repository
----------

The repository hosted in the Gitlab of UGA:
https://gricad-gitlab.univ-grenoble-alpes.fr/python-uga

- Clone the repository with Git with https::

    git clone https://gricad-gitlab.univ-grenoble-alpes.fr/python-uga/py-training-2017.git

- or clone the repository with Git with ssh::

    git clone git@gricad-gitlab.univ-grenoble-alpes.fr:python-uga/py-training-2017.git

- or clone the repository with Mercurial (and the extension hg-git, as explained
  `here <http://fluiddyn.readthedocs.io/en/latest/mercurial_bitbucket.html>`_),
  with the same commands but replacing `git` by `hg`::

    hg clone https://gricad-gitlab.univ-grenoble-alpes.fr/python-uga/py-training-2017.git

  or::

    hg clone git@gricad-gitlab.univ-grenoble-alpes.fr:python-uga/py-training-2017.git


Authors
-------

Pierre Augier (LEGI), Cyrille Bonamy (LEGI), Eric Maldonado (Irstea), Franck
Thollard (ISTERRE), Christophe Picard (LJK), Loïc Huder (ISTerre)
